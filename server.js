const express = require("express");
const cors = require("cors");
const cita = require("./citas.json")

const app = express();

app.use(cors());

app.get("/", (req, res) => {
    res.send("<h1>Benvingut a la API de Contactes!</h1>");
});

app.get("/cita",cors(), (req, res) => {

    const cita_random = cita[Math.floor(Math.random() * cita.length)]

    res.send(cita_random)
});

const PORT = 3003;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});